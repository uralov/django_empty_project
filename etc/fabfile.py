from fabric.api import env, run, sudo, cd, execute, prefix

env.roledefs = {
    'production': [
        'user@server:port',     # ssh access
    ],
    'test': [
        'user@server:port',     # ssh access
    ],
}

PROJECT_NAME = ''
BASE_DIR = ''
PROJECT_DIR = '{0}/{1}'.format(BASE_DIR, PROJECT_NAME)
ENV_ACTIVATE = '{0}/bin/activate'.format(BASE_DIR)


def deploy_test():
    deploy(role='test')


def deploy_production():
    deploy(role='production')


def deploy(role):
    execute(update_project, role=role)
    execute(migrate_databases, role=role)
    execute(collect_static, role=role)
    execute(restart_project, role=role)


def update_project():
    with cd(PROJECT_DIR):
        run('git pull')


def restart_project():
    sudo('supervisorctl restart {0}'.format(PROJECT_NAME))


def collect_static():
    with cd(PROJECT_DIR), prefix('source {0}'.format(ENV_ACTIVATE)):
        run('echo "yes" | python src/manage.py collectstatic')


def migrate_databases():
    with cd(PROJECT_DIR), prefix('source {0}'.format(ENV_ACTIVATE)):
        run('python src/manage.py migrate')


def install_requirements(update=True):
    with cd(PROJECT_DIR), prefix('source {0}'.format(ENV_ACTIVATE)):
        if update:
            run('pip install -U -r requirements.txt')
        else:
            run('pip install -r requirements.txt')


def uninstall_requirements(packages=None):
    if packages:
        with cd(PROJECT_DIR), prefix('source {0}'.format(ENV_ACTIVATE)):
            run('pip uninstall -y {0}'.format(packages))
